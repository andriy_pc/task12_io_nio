package mvc.view;

import java.util.ArrayList;

public class Viewer {
    private ArrayList<String> menu;

    public Viewer() {
        menu = new ArrayList<>();
        init();
    }

    private void init() {
        menu.add("0 quit");
        menu.add("1 check serialization");
        menu.add("2 check performance with/without buffer");
        menu.add("3 check directory content");
    }

    public void display() {
        for(String s : menu) {
            System.out.println(s);
        }
    }
}
