package mvc;

import units.Ship;

import javax.xml.crypto.Data;
import java.io.*;
import java.util.Scanner;

public class Model {
    private Ship deserialize;

    private long timeWithoutBuffer; //NanoSeconds
    private long timeWithBuffer1;
    private long timeWithBuffer2;

    public void serialize(Ship serial) throws IOException {

        try (   FileOutputStream fout = new FileOutputStream("serialized\\Ship.bin");
                ObjectOutputStream out = new ObjectOutputStream(fout);
                ) {
            out.writeObject(serial);
        }
    }
    public void deserialize() {
        try (
                FileInputStream fin = new FileInputStream("serialized\\Ship.bin");
                ObjectInputStream in = new ObjectInputStream(fin);
                ) {
            Ship result = (Ship) in.readObject();
            setDeserialize(result);
        } catch (IOException e) {
            //Loggers!
        } catch (ClassNotFoundException e) {
            //Loggers!
        }
    }
    public void setDeserialize(Ship s) {
        deserialize = s;
    }
    public Ship getDeserialize() {
        return deserialize;
    }

    public void readingWithoutBuffer() {
        long start = System.nanoTime();
        File file = new File("source\\book.pdf");
        try(
                InputStream in = new FileInputStream(file);) {
            int i = in.read();
            int count = 1;
            while(i != -1) {
                i = in.read();
                count += 1;
            }
            System.out.println("count: " + count);
        } catch(FileNotFoundException e) {
            e.printStackTrace();
        } catch(IOException e) {
            e.printStackTrace();
        }
        long currency = System.nanoTime() - start;
        timeWithoutBuffer = currency;
        System.out.println("time: " + timeWithoutBuffer);
    }
    public long getTimeWithoutBuffer() {
        return timeWithoutBuffer;
    }
    public void readingWithBuffer() {
        long start = System.nanoTime();
        File file = new File("source\\book.pdf");
        try(
                InputStream in = new FileInputStream(file);
                BufferedInputStream bin1 = new BufferedInputStream(in, 1024);
                BufferedInputStream bin2 = new BufferedInputStream(in, 2048);
                DataInputStream d1 = new DataInputStream(bin1);
                DataInputStream d2 = new DataInputStream(bin2);
                ) {
            byte b;
            int count = 0;
            while(d1.readByte() != -1) {
                b = d1.readByte();
                count += 1;
            }
            timeWithBuffer1 = System.nanoTime() - start;

            start = System.nanoTime();
            count = 0;
            while(d1.readByte() != -1) {
                b = d2.readByte();
                count += 1;
            }
            timeWithBuffer2 = System.nanoTime() - start;
        } catch(FileNotFoundException e) {
            e.printStackTrace();
        } catch(IOException e) {
            e.printStackTrace();
        }
    }
    public long getTimeWithBuffer1() {
        return timeWithBuffer1;
    }
    public long getTimeWithBuffer2() {
        return timeWithBuffer2;
    }

    public void contentOfFolder(String path) {

        File file = new File(path);
        if(!file.isDirectory()) {
            //
            throw new RuntimeException("It should be directory!");
        }
        File[] files = file.listFiles();
        for(File f : files) {
            System.out.println(f.getName());
            System.out.println("\tcan read: " + f.canRead() +
                    ", can write: " + f.canWrite() +
                    " is directory: " + f.isDirectory());
        }
        setDirectory(files);
    }
    private void setDirectory(File[] files) {
        Scanner in = new Scanner(System.in);
        File file = null;
        System.out.println("Please insert directory name to open");
        System.out.println("insert \"q\" to quit");
        String dirName = in.next(".+");
        if(!dirName.equals("q")) {
            for(File f : files) {
                if(f.getName().equals(dirName)) {
                    file = f;
                    break;
                }
            }
            if(file == null) {
                throw new RuntimeException("File not found!");
            }
            contentOfFolder(file.getPath());
        }
        return;
    }
}
