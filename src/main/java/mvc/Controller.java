package mvc;

import mvc.view.Display;
import mvc.view.Viewer;
import units.Droid;
import units.Ship;

import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Random;
import java.util.Scanner;

public class Controller {

    private Scanner in = new Scanner(System.in);
    private Viewer viewer;
    private Model model;
    private Map<Integer, Display> functions;
    private int choice;

    public Controller(Viewer v, Model m) {
        viewer = v;
        model = m;
        functions = new LinkedHashMap<>();
        init();
    }

    private void init() {
        functions.put(0, this::quit);
        functions.put(1, this::serialize);
        functions.put(2, this::comparePerformance);
        functions.put(3, this::contentOfFolder);
    }

    private void quit() {
        System.exit(0);
    }

    private void serialize() {
        Random rand = new Random(47);
        Ship ser = new Ship(rand.nextInt(16));
        try {
            model.serialize(ser);
        } catch(IOException e) {
            e.printStackTrace();
        }
        System.out.println("Object Ship was serialized. State of this object was: ");
        System.out.println(ser.getShipId());
        for(Droid d : ser.getDroids()) {
            System.out.println(d);
        }
        System.out.println();
        deserialize();
    }

    private void deserialize() {
        model.deserialize();
        Ship s = model.getDeserialize();
        System.out.println("Object Ship was deserialized. State of this object is: ");
        System.out.println(s.getShipId());
        for(Droid d : s.getDroids()) {
            System.out.println(d);
        }
        System.out.println();
    }

    private void comparePerformance() {
        System.out.println("Reading without buffer");
        System.out.println("may take some time...");
        model.readingWithoutBuffer();
        System.out.println("time: "
                + model.getTimeWithoutBuffer());

        System.out.println("Reading with buffer 1024");
        model.readingWithBuffer();
        System.out.println("time: " + model.getTimeWithBuffer1());

        System.out.println("Reading with buffer 2048");
        System.out.println("time: " + model.getTimeWithBuffer2());
    }

    private void contentOfFolder() {
        System.out.println("Please insert path and name of folder");
        String path = in.next(".*");
        model.contentOfFolder(path);
    }

    public void run() {
        System.out.println("Please select action");
        viewer.display();
        choice = in.nextInt();
        functions.get(choice).display();
    }
}
