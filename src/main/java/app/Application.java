package app;

import mvc.Controller;
import mvc.Model;
import mvc.view.Viewer;


public class Application {
    public static void main(String[] args) {
        Controller c = new Controller(new Viewer(), new Model());
        while(true) {
            c.run();
        }
    }
}
