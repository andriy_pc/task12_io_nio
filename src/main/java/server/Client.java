package server;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;
import java.util.Scanner;
import java.util.concurrent.TimeUnit;

import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;

public class Client {
    private static final int BUFFER_SIZE = 2048;
    private static Logger log = LogManager.getLogger();
    private static SocketChannel clientChannel;

    public static void main(String[] args) throws InterruptedException {
        try {
            clientChannel = SocketChannel.open();
            InetSocketAddress address = new InetSocketAddress("localhost", 8054);
            clientChannel.connect(address);

            Scanner in = new Scanner(System.in);
            String sent;
            do {
                System.out.println("Please print something");
                sent = in.nextLine();
                ByteBuffer buff = ByteBuffer.allocate(BUFFER_SIZE);
                buff.put(sent.getBytes());
                buff.flip();
                int res = clientChannel.write(buff);

                clientChannel.read(buff);
                buff.flip();
                if(buff.remaining() == 0) {
                    log.error("Server didn't respond");
                    throw new RuntimeException("Server hangs");
                } else {
                    String respond = new String(buff.array());
                    System.out.println("Respond : " + respond);
                }
            } while(!sent.equals("quit"));

            log.info("leaving channel");
            clientChannel.close();
        } catch (IOException e) {
            for(StackTraceElement t : e.getStackTrace()) {
                log.error(t);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
