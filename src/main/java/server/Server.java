package server;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SelectionKey;
import java.nio.channels.SocketChannel;
import java.util.Iterator;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;

import javax.print.DocFlavor;

public class Server {
    private static final int BUFFER_SIZE = 2048;
    private static Logger log = LogManager.getLogger();
    private static Selector selector;
    private static ServerSocketChannel serverChannel;
    private static InetSocketAddress socketAddress;
    private static String message;

    private static boolean written = false;

    public static void main(String[] args) {
        try {
            log.info("Starting server");
            selector = Selector.open();
            socketAddress = new InetSocketAddress("localhost", 8054);
            serverChannel = ServerSocketChannel.open();
            serverChannel.configureBlocking(false);
            serverChannel.bind(socketAddress);

            SelectionKey selectionServerKey =
                    serverChannel.register(selector, SelectionKey.OP_ACCEPT, null);


            while(true) {
                log.info("Server is running");
                int s = selector.select();

                Set<SelectionKey> keys = selector.selectedKeys();
                Iterator<SelectionKey> iter = keys.iterator();
                while (iter.hasNext()) {
                    SelectionKey key = iter.next();
                    if (key.isAcceptable()) {
                        registerClient(key);
                    } else if (key.isReadable()) {
                        if (readClient(key)) {
                            log.trace("Message received: " + message);
                        }
                    }
                    keys.clear();
                }
            }
        } catch(IOException e) {
            for(StackTraceElement t : e.getStackTrace()) {
                log.error(t);
            }
        }

    }

    public static void registerClient(SelectionKey key) {
        try {
            SocketChannel client = serverChannel.accept();
            log.info("registering client " + client.getLocalAddress());
            client.configureBlocking(false);
            client.register(selector, SelectionKey.OP_READ);
        } catch (IOException e) {
            for(StackTraceElement t : e.getStackTrace()) {
                log.error(t);
            }
        }
    }

    public static boolean readClient(SelectionKey key) {
        try {
            log.info("reading clients message");
            SocketChannel client = (SocketChannel) key.channel();

            ByteBuffer buff = ByteBuffer.allocate(BUFFER_SIZE);
            client.read(buff);
            buff.flip();

            if(buff.remaining() == 0) {
                return false;
            }

            String result = new String(buff.array()).trim();
            write(key);
            if(result.equals("quit")) {
                log.info("Client left the channel");
                client.close();
                return false;
            }

            setMessage(result);
        } catch (IOException e) {
            for(StackTraceElement t : e.getStackTrace()) {
                log.error(t);
            }
        }

        return true;
    }

    public static void write(SelectionKey key) {
        try {
            SocketChannel channel = (SocketChannel) key.channel();
            ByteBuffer bf = ByteBuffer.allocate(BUFFER_SIZE);
            bf.put("Writeln".getBytes());
            bf.flip();
            channel.write(bf);
            written = true;
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public static void setMessage(String m) {
        message = m;
    }
}
