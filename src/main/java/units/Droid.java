package units;

import java.io.Serializable;

public class Droid implements Serializable {
    private int index;
    private transient char key;

    public Droid() {

    }

    public Droid(int i, char k) {
        index = i;
        key = k;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public char getKey() {
        return key;
    }

    public void setKey(char key) {
        this.key = key;
    }

    public String toString() {
        return "Index: " + index + ", key: " + key;
    }
}
