package units;

import java.io.Serializable;
import java.util.Random;

public class Ship implements Serializable {
    private int shipId;
    private Droid[] droids;
    private Random rand;
    public Ship(int id) {
        rand = new Random(47);
        droids = new Droid[] {
                new Droid(rand.nextInt(15), (char)('a'+rand.nextInt(15))),
                new Droid(rand.nextInt(15), (char)('a'+rand.nextInt(15))),
                new Droid(rand.nextInt(15), (char)('a'+rand.nextInt(15))),
                new Droid(rand.nextInt(15), (char)('a'+rand.nextInt(15))),
                new Droid(rand.nextInt(15), (char)('a'+rand.nextInt(15))),
        };
        shipId = id;
    }

    public void setDroids(Droid[] droids) {
        this.droids = droids;
    }
    public Droid[] getDroids() {
        return droids;
    }
    public void setShipId(int id) {
        shipId = id;
    }
    public int getShipId() {
        return shipId;
    }
}
