package mvc;

import mvc.view.Viewer;
import org.junit.jupiter.api.BeforeEach;
import units.*;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.File;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ModelTest {

    Model m;

    @BeforeEach
    void init() {
        m = new Model();
    }

    @Test
    public void testSerialization() {
        Model m = new Model();
        try {
            m.serialize(new Ship(5));
        } catch (IOException e) {
            e.printStackTrace();
        }
        File f = new File("serialized\\Ship.bin");
        assertTrue(f.exists());
    }

    @Test
    public void testDeserializationId() {
        Model m = new Model();
        Ship s = new Ship(5);
        Ship result;
        try {
            m.serialize(s);
            m.deserialize();
        } catch (IOException e) {
            e.printStackTrace();
        }
        result = (Ship) m.getDeserialize();
        for(Droid d : s.getDroids()) {
            System.out.println(d);
        }
        System.out.println();
        for(Droid d : result.getDroids()) {
            System.out.println(d);
        }
        assertEquals(s.getShipId(), result.getShipId());
    }

    @Test
    public void testWithoutBuffer() {
        Model m = new Model();
        m.readingWithoutBuffer();

        assertNotNull(m.getTimeWithoutBuffer());
    }

    @Test
    public void testWithBuffersBuff1() {
        m.readingWithBuffer();
        assertNotNull(m.getTimeWithBuffer1());
    }

    @Test
    public void testWithBuffersBuff2() {
        m.readingWithBuffer();
        assertNotNull(m.getTimeWithBuffer2());
    }

    @Test
    public void testContent() {
        m.contentOfFolder("D:\\tmp");
    }

    @Test
    public void controller() {
        Controller c = new Controller(new Viewer(), new Model());
        c.run();
    }
}